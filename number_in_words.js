
function humanize(num){
    var ones = ['', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan',
                'sepuluh', 'sebelas', 'dua belas', 'tiga belas', 'empat belas', 'lima belas', 'enam belas',
                'tujuh belas', 'delapan belas', 'sembilan belas'];
    var tens = ['', '', 'dua puluh', 'tiga puluh', 'empat puluh', 'lima puluh', 'enam puluh', 'tujuh puluh', 'delapan puluh','sembilan puluh'];
  
    //if number negative
    if(num < 0) return 'Negative numbers are not supported.';

    //if number zero
    if (num === 0) return 'zero';
  
    //the case of 1 - 20
    if (num < 20) {
      return ones[num];
    }

    var numString = num.toString();
    // console.log("Num_string:", numString);

    // console.log("Num_string_length:", numString.length);
    // console.log('Apasih1:', tens[numString[0]]);
    // console.log('Apasih2:', tens[2]);
    // console.log('Apasih3:', numString[0]);
    // console.log('Apasih4:', numString[1]);
    if (numString.length === 2) {
      return tens[numString[0]] + ' ' + ones[numString[1]];
    }
  

    // console.log("Num_string_length:", numString.length);
    // console.log('Apasih0:', numString[0]);
    // console.log('Apasih1:', numString[1]);
    // console.log('Apasih2:', numString[2]);
    //100 and more
    if (numString.length == 3) {
      if (numString[1] === '0' && numString[2] === '0')
        return ones[numString[0]] + ' ratus';
      else
        return ones[numString[0]] + ' ratus ' + humanize(+(numString[1] + numString[2]));
        // console.log('Isinya apaan:', humanize(+(numString[1] + numString[2])));
    }

    // console.log("Num_string_length:", numString.length);
    // console.log('Apasih0:', numString[0]);
    // console.log('Apasih1:', numString[1]);
    // console.log('Apasih2:', numString[2]);
    // console.log('Apasih3:', numString[3]);

    if (numString.length === 4) {
      var three_digit = +(numString[1] + numString[2] + numString[3]);
      //console.log("Kampret:", three_digit);
      if (three_digit === 0) return ones[numString[0]] + ' ribu';
      //console.log("hasil:", humanize(three_digit));
      if (three_digit < 100) return ones[numString[0]] + ' ribu ' + humanize(three_digit);
      //more than 100
      return ones[numString[0]] + ' ribu ' + humanize(three_digit);
    }
  }
console.log(humanize(2525));
